import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const double defaultMargin = 24.0;

// Note: Heigh and width

Size displaySize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double displayHeight(BuildContext context) {
  return displaySize(context).height;
}

double displayWidth(BuildContext context) {
  return displaySize(context).width;
}

// Note: Colors
Color primaryColor = Color(0xff28c870);
Color secondaryColor = Color(0xff20a15a);
Color thirdColor = Color(0xffC3C8D8);
Color fourthColor = Color(0xff092a18);
Color fifthhColor = Color(0xffEEFBEF);
Color gradient1 = Color(0xffE4F2F7);
Color gradient2 = Color(0xffFFFFFF);

Color transparentColor = Colors.transparent;
FontWeight light = FontWeight.w300;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w700;
FontWeight bold = FontWeight.bold;

TextStyle primaryTextStyle = GoogleFonts.montserrat();
