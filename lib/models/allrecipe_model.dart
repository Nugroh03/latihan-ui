class AllRecipeModel {
  String? id;
  String? title;
  String? time;
  String? serving;
  String? dificulty;
  String? image;

  AllRecipeModel({
    this.id,
    this.title,
    this.time,
    this.serving,
    this.dificulty,
    this.image,
  });

  AllRecipeModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    time = json['time'];
    serving = json['serving'];
    dificulty = json['dificulty'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'time': time,
      'serving': serving,
      'dificulty': dificulty,
      'images': image,
    };
  }
}
