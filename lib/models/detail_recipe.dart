class DetailRecipe {
  String? id;
  String? title;
  String? time;
  String? author;
  String? serving;
  String? dificulty;
  String? description;
  List? ingredients;
  List? steps;
  String? image;

  DetailRecipe({
    this.id,
    this.title,
    this.time,
    this.author,
    this.serving,
    this.dificulty,
    this.description,
    this.ingredients,
    this.steps,
    this.image,
  });

  DetailRecipe.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    time = json['time'];
    author = json['author'];
    serving = json['serving'];
    dificulty = json['dificulty'];
    description = json['description'];
    ingredients = json['ingredients'];
    steps = json['steps'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'time': time,
      'author': author,
      'serving': serving,
      'dificulty': dificulty,
      'description': description,
      'ingredientss': ingredients,
      'steps': steps,
      'images': image,
    };
  }
}
