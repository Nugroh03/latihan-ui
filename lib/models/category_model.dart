class CategoryModel {
  String? categoryId;
  String? categoryName;
  String? categoryItems;

  CategoryModel({
    this.categoryId,
    this.categoryName,
    this.categoryItems,
  });

  CategoryModel.fromJson(Map<String, dynamic> json) {
    categoryId = json['categoryId'];
    categoryName = json['categoryName'];
    categoryItems = json['categoryItems'];
  }

  Map<String, dynamic> toJson() {
    return {
      'categoryId': categoryId,
      'categoryName': categoryName,
      'categoryItems': categoryItems,
    };
  }
}
