import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edubox/models/allrecipe_model.dart';
import 'package:edubox/models/category_model.dart';
import 'package:edubox/models/detail_recipe.dart';
import 'package:http/http.dart' as http;

import '../api.dart';

class AllRecipeService {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  // Future<List<AllRecipeModel?>> allrecipes() async {
  //   var url = ('$apiallrecipe');
  //   //var headers = {'Content-Type': 'aplication/json'};

  //   var response = await http.get(
  //     Uri.parse(url),
  //     //headers: headers,
  //   );

  //   if (response.statusCode == 200) {
  //     List data = jsonDecode(response.body)['data'];
  //     List<AllRecipeModel?> allrecipe = [];

  //     for (var item in data) {
  //       allrecipe.add(AllRecipeModel?.fromJson(item));
  //     }

  //     return allrecipe;
  //   } else {
  //     throw Exception('Gagal Get all recipe');
  //   }
  // }

  // Future<List<CategoryModel?>> allcategories() async {
  //   var url = ('$apiallcategories');
  //   //var headers = {'Content-Type': 'aplication/json'};

  //   var response = await http.get(
  //     Uri.parse(url),
  //     //headers: headers,
  //   );

  //   print(response.body);

  //   if (response.statusCode == 200) {
  //     List data = jsonDecode(response.body)['data'];
  //     List<CategoryModel?> allcategories = [];

  //     for (var item in data) {
  //       allcategories.add(CategoryModel?.fromJson(item));
  //     }
  //     print("recipelist" + allcategories.toString());
  //     return allcategories;
  //   } else {
  //     throw Exception('Gagal Get all recipe');
  //   }
  // }

  // Future<List<AllRecipeModel?>> getcategories(categoryId) async {
  //   var url = ('$apicategories' + '$categoryId');
  //   //var headers = {'Content-Type': 'aplication/json'};

  //   var response = await http.get(
  //     Uri.parse(url),
  //     //headers: headers,
  //   );

  //   if (response.statusCode == 200) {
  //     List data = jsonDecode(response.body)['data'];
  //     List<AllRecipeModel?> categories = [];

  //     for (var item in data) {
  //       categories.add(AllRecipeModel?.fromJson(item));
  //     }

  //     return categories;
  //   } else {
  //     throw Exception('Gagal Get all recipe');
  //   }
  // }

  //Get data in firebase

  Future<List<CategoryModel?>> getcategoryFirebase() async {
    try {
      var datacategory = await firestore
          .collection('categories')
          .get()
          .then((QuerySnapshot list) {
        var result =
            list.docs.map<CategoryModel?>((DocumentSnapshot datacategory) {
          //print('cobalist' + datacategory.data().toString());

          return CategoryModel.fromJson(
              datacategory.data()! as Map<String, dynamic>);
        }).toList();

        return result;
      });
      return datacategory;
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<List<AllRecipeModel?>> getItemsCategory({String? id}) async {
    //print('coba' + userId.toString());
    try {
      var items = firestore
          .collection('categoryItems/$id/items/')
          .get()
          .then((QuerySnapshot list) {
        var result = list.docs.map<AllRecipeModel?>((DocumentSnapshot items) {
          //print('cobalist' + wishlist.data().toString());

          return AllRecipeModel.fromJson(items.data()! as Map<String, dynamic>);
        }).toList();

        //print('coba' + result.toString());
        return result;
      });

      return items;
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<List<DetailRecipe?>> getAllRecipe() async {
    try {
      var recipe = await firestore
          .collection('detailRecipe')
          .get()
          .then((QuerySnapshot list) {
        var result = list.docs.map<DetailRecipe?>((DocumentSnapshot recipe) {
          //print('cobalist' + recipe.data().toString());

          return DetailRecipe.fromJson(recipe.data()! as Map<String, dynamic>);
        }).toList();

        return result;
      });
      return recipe;
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<DetailRecipe> getdetailrecipe({String? id}) async {
    try {
      var details = await firestore.collection('detailRecipe').doc(id).get();

      DetailRecipe detailrecipe = DetailRecipe.fromJson(details.data()!);

      print('itemsdetail ' + detailrecipe.id.toString());
      return detailrecipe;
    } catch (e) {
      throw Exception('Pesan Gagal Dikirim');
    }
  }
}
