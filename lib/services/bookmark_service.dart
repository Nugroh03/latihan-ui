import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:edubox/models/detail_recipe.dart';

class BookmarkService {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<DetailRecipe?> addBookmark(
    String imageRecipe,
    String userId,
    DetailRecipe recipe,
  ) async {
    try {
      firestore.collection('bookmark/').doc(userId.toString()).set({
        'id': recipe.id,
        'title': recipe.title,
        'time': recipe.time,
        'serving': recipe.serving,
        'dificulty': recipe.dificulty,
        'image': imageRecipe,
        'author': recipe.author,
        'ingredients': recipe.ingredients,
        'steps': recipe.steps,
        'description': recipe.description
      }).then((value) {
        firestore.collection('bookmark/$userId/listbookmark/').add({
          'id': recipe.id,
          'title': recipe.title,
          'time': recipe.time,
          'serving': recipe.serving,
          'dificulty': recipe.dificulty,
          'image': imageRecipe,
          'author': recipe.author,
          'ingredients': recipe.ingredients,
          'steps': recipe.steps,
          'description': recipe.description
        }).then((value) => print('Pesan Berhasil Dikirim'));
      });
    } catch (e) {
      throw Exception('Pesan Gagal Dikirim');
    }
  }

  Stream<List<DetailRecipe?>> getBookmark({int? userId}) {
    try {
      return firestore
          .collection('bookmark/$userId/listbookmark/')
          .snapshots()
          .map((QuerySnapshot list) {
        var result = list.docs.map<DetailRecipe?>((DocumentSnapshot bookmark) {
          // print(bookmark.data());
          return DetailRecipe.fromJson(
              bookmark.data()! as Map<String, dynamic>);
        }).toList();

        // result.sort(
        //   (ProductModel? b, ProductModel? a) =>
        //       a!.createdAt!.compareTo(b!.createdAt!),
        // );
        return result;
      });
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<List<DetailRecipe?>?> getBookmarkfuture({String? userId}) async {
    //print('coba' + userId.toString());
    try {
      var bookmark = firestore
          .collection('bookmark/$userId/listbookmark/')
          .get()
          .then((QuerySnapshot list) {
        var result = list.docs.map<DetailRecipe?>((DocumentSnapshot bookmark) {
          //print('cobalist' + bookmark.data().toString());

          return DetailRecipe.fromJson(
              bookmark.data()! as Map<String, dynamic>);
        }).toList();

        // result.sort(
        //   (ProductModel? b, ProductModel? a) =>
        //       a!.createdAt!.compareTo(b!.createdAt!),
        // );
        //print('coba' + result.toString());
        return result;
      });

      return bookmark;
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<DetailRecipe?> delete(String userId, DetailRecipe product) async {
    try {
      FirebaseFirestore.instance
          .collection("bookmark/$userId/listbookmark/")
          .where("id", isEqualTo: product.id)
          .get()
          .then((value) {
        value.docs.forEach((element) {
          FirebaseFirestore.instance
              .collection("bookmark/$userId/listbookmark/")
              .doc(element.id)
              .delete()
              .then((value) {
            print("Success!");
          });
        });
      });
    } catch (e) {
      throw Exception('Pesan Gagal Dikirim');
    }
  }
}
