import 'package:cached_network_image/cached_network_image.dart';
import 'package:edubox/constans.dart';
import 'package:edubox/models/allrecipe_model.dart';
import 'package:edubox/models/detail_recipe.dart';
import 'package:edubox/pages/detail_recipe_page.dart';
import 'package:edubox/providers/recipe_provider.dart';
import 'package:edubox/services/recipe_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoriesSlideWidget extends StatefulWidget {
  const CategoriesSlideWidget({Key? key, this.dataitem}) : super(key: key);
  final AllRecipeModel? dataitem;

  @override
  State<CategoriesSlideWidget> createState() => _CategoriesSlideWidgetState();
}

class _CategoriesSlideWidgetState extends State<CategoriesSlideWidget> {
  DetailRecipe? detailrecipe;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    detailItems(widget.dataitem!.id);
  }

  detailItems(String? id) async {
    DetailRecipe? detail = await AllRecipeService().getdetailrecipe(id: id);
    detailrecipe = detail;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailsRecipePage(
                      items: detailrecipe,
                      image: widget.dataitem!.image,
                    )));
      },
      child: Container(
        width: 180,
        margin: EdgeInsets.only(right: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CachedNetworkImage(
              imageUrl: widget.dataitem!.image!,
              imageBuilder: (context, imageProvider) => Container(
                width: 180,
                height: 220,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  image:
                      DecorationImage(image: imageProvider, fit: BoxFit.cover),
                ),
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        height: 100,
                        width: displayWidth(context),
                        margin: EdgeInsets.only(left: 10, right: 10, top: 10),
                        padding: EdgeInsets.only(top: 10),
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.8),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          children: [
                            Text(
                              widget.dataitem!.title!,
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: primaryTextStyle.copyWith(
                                  fontSize: 18,
                                  color: gradient2,
                                  fontWeight: bold),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 30,
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      widget.dataitem!.time!,
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                      overflow: TextOverflow.ellipsis,
                                      style: primaryTextStyle.copyWith(
                                          fontSize: 12,
                                          color: gradient2,
                                          fontWeight: bold),
                                    ),
                                  ),
                                  VerticalDivider(
                                    thickness: 2,
                                    color: primaryColor,
                                  ),
                                  Expanded(
                                    child: Text(
                                      widget.dataitem!.dificulty!,
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                      overflow: TextOverflow.ellipsis,
                                      style: primaryTextStyle.copyWith(
                                          fontSize: 12,
                                          color: gradient2,
                                          fontWeight: bold),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              placeholder: (context, url) => Container(
                  height: 90,
                  width: 90,
                  padding: EdgeInsets.all(20),
                  child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(
              height: 10,
            ),
            // Container(
            //   padding: EdgeInsets.symmetric(horizontal: 5),
            //   child: Text(
            //     widget.dataitem!.title!,
            //     maxLines: 2,
            //     overflow: TextOverflow.ellipsis,
            //     style: primaryTextStyle.copyWith(
            //       fontSize: 14,
            //       color: primaryColor,
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
