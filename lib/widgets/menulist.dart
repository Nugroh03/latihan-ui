import 'package:cached_network_image/cached_network_image.dart';
import 'package:edubox/constans.dart';
import 'package:edubox/models/detail_recipe.dart';
import 'package:edubox/pages/detail_recipe_page.dart';
import 'package:edubox/providers/user_provider.dart';
import 'package:edubox/providers/wishlist_provider.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MenuList extends StatelessWidget {
  const MenuList({Key? key, this.data}) : super(key: key);

  final DetailRecipe? data;
  @override
  Widget build(BuildContext context) {
    BookmarkProvider bookmarkProvider = Provider.of<BookmarkProvider>(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailsRecipePage(
                      items: data,
                      image: data!.image,
                    )));
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              blurRadius: 8,
              offset: Offset(4, 8), // Shadow position
            ),
          ],
        ),
        margin: EdgeInsets.only(top: 5, bottom: 5),
        child: Row(
          children: [
            CachedNetworkImage(
              imageUrl: data!.image!,
              imageBuilder: (context, imageProvider) => Container(
                width: displayWidth(context) * 0.3,
                height: 120,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      bottomLeft: Radius.circular(15)),
                  image:
                      DecorationImage(image: imageProvider, fit: BoxFit.cover),
                ),
              ),
              placeholder: (context, url) => Container(
                  height: 80,
                  width: 80,
                  padding: EdgeInsets.all(20),
                  child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data!.title!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 16, color: primaryColor, fontWeight: bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.person,
                              color: secondaryColor,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "${data!.author}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: primaryTextStyle.copyWith(
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.group,
                              color: secondaryColor,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "${data!.serving}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: primaryTextStyle.copyWith(
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.speed_outlined,
                              color: secondaryColor,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "${data!.dificulty}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: primaryTextStyle.copyWith(
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
