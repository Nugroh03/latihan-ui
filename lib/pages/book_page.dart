import 'package:edubox/models/detail_recipe.dart';
import 'package:edubox/services/recipe_service.dart';
import 'package:edubox/widgets/menulist.dart';
import 'package:flutter/material.dart';

import '../constans.dart';

class BookPage extends StatefulWidget {
  const BookPage({
    Key? key,
  }) : super(key: key);

  @override
  State<BookPage> createState() => _BookPageState();
}

class _BookPageState extends State<BookPage> {
  ScrollController _scrollController = new ScrollController();

  bool isLoading = false;

  List? names = [];

  @override
  void initState() {
    this._getMoreData();
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _getMoreData();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _getMoreData() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      final response = await AllRecipeService().getAllRecipe();
      List tempList = [];
      for (int i = 0; i < response.length; i++) {
        tempList.add(response[i]);
      }

      setState(() {
        isLoading = false;
        names!.addAll(tempList);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            child: Container(
              height: 70,
              padding: EdgeInsets.only(left: 20, right: 20, top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Book Menu',
                          style: primaryTextStyle.copyWith(
                              fontWeight: bold, fontSize: 22),
                        ),
                        Container(
                          child: Row(
                            children: [
                              Image.asset(
                                'assets/logo.png',
                                height: 30,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            preferredSize: Size.fromHeight(70)),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: names!.length + 1,
            itemBuilder: (BuildContext context, int index) {
              if (index == names!.length) {
                return Container(
                  child: Center(
                    child: Opacity(
                      opacity: isLoading ? 1.0 : 00,
                      child: CircularProgressIndicator(),
                    ),
                  ),
                );
              } else {
                return MenuList(
                  data: names![index],
                );
              }
            },
            controller: _scrollController,
          ),
        ),

        // SingleChildScrollView(
        //   child: Container(
        //     padding: EdgeInsets.symmetric(horizontal: 20),
        //     margin: EdgeInsets.only(top: 20),
        //     child: Column(
        //         mainAxisAlignment: MainAxisAlignment.start,
        //         crossAxisAlignment: CrossAxisAlignment.start,
        //         children: widget.recipes!.map((item) {
        //         return MenuList(
        //           data: item,
        //         );
        //       }).toList()),
        // ),
        // ),
      ),
    );
  }
}
