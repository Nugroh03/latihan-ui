import 'dart:async';

import 'package:edubox/providers/recipe_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getallrecipe();
    getallcategories();
  }

  getallrecipe() async {
    await Provider.of<RecipeProvider>(context, listen: false).getAllRecipe();
  }

  getallcategories() async {
    await Provider.of<RecipeProvider>(context, listen: false)
        .getCategoriesFirebase();
    Navigator.pushNamed(context, '/onboarding');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(
          'assets/logo.png',
          height: 100,
        ),
      ),
    );
  }
}
