import 'package:edubox/pages/detail_recipe_page.dart';
import 'package:edubox/providers/recipe_provider.dart';
import 'package:edubox/providers/wishlist_provider.dart';
import 'package:edubox/widgets/menulist.dart';
import 'package:edubox/widgets/saverecipe_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

import '../constans.dart';

class SaveRecipe extends StatefulWidget {
  SaveRecipe({Key? key, this.homepage}) : super(key: key);

  final Function? homepage;

  @override
  _SaveRecipeState createState() => _SaveRecipeState();
}

class _SaveRecipeState extends State<SaveRecipe> {
  getallrecipe(String id) async {
    await Provider.of<RecipeProvider>(context, listen: false)
        .detailitemcategory(id);
  }

  @override
  Widget build(BuildContext context) {
    BookmarkProvider bookmarkProvider = Provider.of<BookmarkProvider>(context);

    Widget emptyBookmark() {
      return Expanded(
        child: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.bookmark_remove,
                  size: 50,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Belum ada resep yang tersimpan',
                  textAlign: TextAlign.center,
                  style:
                      primaryTextStyle.copyWith(fontSize: 22, fontWeight: bold),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Silahkan pilih Recipe terlebih dahulu!',
                  style: primaryTextStyle.copyWith(
                    fontSize: 18,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: secondaryColor),
                      onPressed: () async {
                        widget.homepage!();
                      },
                      child: Text(
                        'Recipe',
                        style: primaryTextStyle.copyWith(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: gradient2,
                        ),
                      )),
                )
              ],
            ),
          ),
        ),
      );
    }

    Widget content() {
      return Container(
        child: Column(
            children: bookmarkProvider.bookmark
                .map((bookmark) => SaveRecipeWidget(
                      data: bookmark,
                    ))
                .toList()),
      );
    }

    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
            child: Container(
              height: 70,
              padding: EdgeInsets.only(left: 20, right: 20, top: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Bookmark',
                          style: primaryTextStyle.copyWith(
                              fontWeight: bold, fontSize: 22),
                        ),
                        Container(
                          child: Row(
                            children: [
                              Image.asset(
                                'assets/logo.png',
                                height: 30,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            preferredSize: Size.fromHeight(70)),
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            margin: EdgeInsets.only(top: 20),
            child: Column(children: [
              bookmarkProvider.bookmark.length == 0
                  ? emptyBookmark()
                  : content()
            ])),
      ),
    );
  }
}
