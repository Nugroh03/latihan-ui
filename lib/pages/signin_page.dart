import 'package:edubox/constans.dart';
import 'package:edubox/models/user_model.dart';
import 'package:edubox/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

class SigninPage extends StatefulWidget {
  SigninPage({
    Key? key,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<SigninPage> {
  final TextEditingController _emailController =
      TextEditingController(text: "nugroho@gmail.com");
  final TextEditingController _passwordController =
      TextEditingController(text: "123456");
  bool isShow = true;
  bool isLoading = false;
  showPass() {
    setState(() {
      isShow = !isShow;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    UserProvider authProvider = Provider.of<UserProvider>(context);

    Widget signin() {
      return ElevatedButton(
          style: ElevatedButton.styleFrom(primary: secondaryColor),
          onPressed: () async {
            setState(() {
              isLoading = true;
            });

            if (await authProvider.login(
              email: _emailController.text,
              password: _passwordController.text,
            )) {
              Navigator.pushNamed(context, '/navigation');
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title:
                          Text('Login Gagal,\nCek kembali email dan password!'),
                      titleTextStyle: primaryTextStyle.copyWith(fontSize: 14),
                      actions: <Widget>[
                        TextButton(
                          child: Text(
                            'OK',
                            style: TextStyle(fontSize: 16),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop(false);
                          },
                        ),
                      ],
                    );
                  });
            }

            setState(() {
              isLoading = false;
            });
          },
          child: Text('MASUK'));
    }

    Widget inputlogin() {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Email',
              style: primaryTextStyle.copyWith(
                  fontSize: 16, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 7),
            Container(
              child: TextFormField(
                controller: _emailController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                  hintText: 'Masukkan Email',
                  hintStyle: primaryTextStyle.copyWith(fontSize: 16),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: BorderSide(
                      color: Color(0xffC3C3C3),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: BorderSide(
                      color: Color(0xffC3C3C3),
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Password',
              style: primaryTextStyle.copyWith(
                  fontSize: 16, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 7),
            Container(
              child: TextFormField(
                obscureText: isShow,
                controller: _passwordController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                  hintText: 'Masukkan Password',
                  suffixIcon: InkWell(
                    onTap: () {
                      showPass();
                    },
                    child: Icon(
                      isShow
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_outlined,
                      color: primaryColor,
                    ),
                  ),
                  hintStyle: primaryTextStyle.copyWith(fontSize: 16),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: BorderSide(
                      color: Color(0xffC3C3C3),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: BorderSide(
                      color: Color(0xffC3C3C3),
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 85,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Recipemu',
                      style: primaryTextStyle.copyWith(
                          fontSize: 48, fontWeight: FontWeight.bold),
                    ),
                    Image.asset(
                      'assets/Asset 1.png',
                      height: 100,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Login untuk dapat melihat resep masakan favoritemu',
                style: primaryTextStyle.copyWith(
                  fontSize: 16,
                ),
              ),
              SizedBox(
                height: 50,
              ),
              inputlogin(),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Text(
                  'Lupa password ?',
                  style: primaryTextStyle.copyWith(
                      fontSize: 16, color: Colors.red),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60,
                child: Center(
                    child: isLoading
                        ? Container(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                              valueColor: AlwaysStoppedAnimation(thirdColor),
                            ),
                          )
                        : signin()),
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                width: width - 60,
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Belum punya akun? Klik ',
                      style: primaryTextStyle.copyWith(
                        fontSize: 16,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, "/register");
                      },
                      child: Text(
                        'Daftar ',
                        style: primaryTextStyle.copyWith(
                            fontSize: 16,
                            color: secondaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              )
            ],
          ),
        ),
      ),
    );
  }
}
