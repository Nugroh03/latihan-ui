import 'package:edubox/constans.dart';
import 'package:edubox/models/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key, this.user}) : super(key: key);

  final UserModel? user;

  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    void alert() async {
      return await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Yakin anda ingin keluar ?'),
              actions: <Widget>[
                TextButton(
                  child: Text(
                    'Tidak',
                    style: primaryTextStyle.copyWith(fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                TextButton(
                  child: Text(
                    'Ya',
                    style: primaryTextStyle.copyWith(fontSize: 16),
                  ),
                  onPressed: () {
                    _signOut().then((value) =>
                        Navigator.pushNamedAndRemoveUntil(
                            context, '/login', (route) => false));
                  },
                ),
              ],
            );
          });
    }

    Widget logOut() {
      return Container(
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: secondaryColor),
            onPressed: () {
              alert();
            },
            child: Text(
              'Keluar',
              style: primaryTextStyle.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: gradient2,
              ),
            )),
      );
    }

    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: 100,
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: fourthColor,
                shape: BoxShape.circle,
              ),
              child: Image.asset('assets/logo.png'),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "${user!.name}",
              style: primaryTextStyle.copyWith(fontSize: 25, fontWeight: bold),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "${user!.telephone}",
              style: primaryTextStyle.copyWith(
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 60,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Hobi',
                    style: primaryTextStyle.copyWith(
                        fontSize: 20, fontWeight: bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "${user!.hobi}",
                    style: primaryTextStyle.copyWith(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Email',
                    style: primaryTextStyle.copyWith(
                        fontSize: 20, fontWeight: bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "${user!.email}",
                    style: primaryTextStyle.copyWith(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Pekerjaan',
                    style: primaryTextStyle.copyWith(
                        fontSize: 20, fontWeight: bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "${user!.pekerjaan}",
                    style: primaryTextStyle.copyWith(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            logOut()
          ],
        ),
      ),
    );
  }
}
