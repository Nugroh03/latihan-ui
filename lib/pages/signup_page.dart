import 'package:edubox/constans.dart';
import 'package:edubox/providers/user_provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({
    Key? key,
  }) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController _noTelephoneController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _hobiController = TextEditingController();
  final TextEditingController _pekerjaanController = TextEditingController();
  final TextEditingController _pinController = TextEditingController();
  bool isShow = true;
  String? verId;
  String? phone;
  bool codeSent = false;
  String? otp;
  String kodephone = "+62";

  bool isLoading = false;
  showPass1() {
    setState(() {
      isShow = !isShow;
    });
  }

  Future<void> verifyPin(String pin) async {
    PhoneAuthCredential credential =
        PhoneAuthProvider.credential(verificationId: verId!, smsCode: pin);

    try {
      await FirebaseAuth.instance.signInWithCredential(credential);
      final snackBar = SnackBar(content: Text("Login Success"));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } on FirebaseAuthException catch (e) {
      final snackBar = SnackBar(content: Text("${e.message}"));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> verifyPhone() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phone!,
        verificationCompleted: (PhoneAuthCredential credential) async {
          await FirebaseAuth.instance.signInWithCredential(credential);
          final snackBar = SnackBar(content: Text("OTP telah dikirim"));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        verificationFailed: (FirebaseAuthException e) {
          final snackBar = SnackBar(content: Text("${e.message}"));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        codeSent: (String verficationId, int? resendToken) {
          setState(() {
            codeSent = true;
            verId = verficationId;
          });
          print(verId);
        },
        codeAutoRetrievalTimeout: (String verificationId) {
          setState(() {
            verId = verificationId;
          });
        },
        timeout: Duration(seconds: 60));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    UserProvider authProvider = Provider.of<UserProvider>(context);

    Widget header() {
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 35,
            ),
            Text(
              'Daftar',
              style: primaryTextStyle.copyWith(
                  fontSize: 36, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'untuk dapat melihat menu',
              style: primaryTextStyle.copyWith(
                fontSize: 16,
              ),
            ),
            SizedBox(
              height: 12,
            ),
          ],
        ),
      );
    }

    Widget content() {
      return Container(
        child: codeSent
            ? Container(
                height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextFormField(
                      onChanged: (value) {
                        setState(() {
                          otp = _pinController.text;
                        });
                        print(otp);
                      },
                      controller: _pinController,
                      decoration: InputDecoration(
                          hintText: "OTP",
                          border: OutlineInputBorder(borderSide: BorderSide())),
                    ),
                    ElevatedButton(
                        onPressed: () async {
                          PhoneAuthCredential credential =
                              PhoneAuthProvider.credential(
                                  verificationId: verId!, smsCode: otp!);

                          try {
                            await FirebaseAuth.instance
                                .signInWithCredential(credential);
                            if (await authProvider.register(
                                name: _nameController.text,
                                email: _emailController.text,
                                telephone: _noTelephoneController.text,
                                password: _passwordController.text,
                                hobi: _hobiController.text,
                                pekerjaan: _pekerjaanController.text)) {
                              Navigator.pushNamedAndRemoveUntil(
                                  context, '/navigation', (route) => false);
                            } else {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text(
                                          'Gagal Mendaftar,\nMasukkan email dengan benar!'),
                                      titleTextStyle: primaryTextStyle.copyWith(
                                          fontSize: 14),
                                      actions: <Widget>[
                                        TextButton(
                                          child: Text(
                                            'OK',
                                            style: TextStyle(fontSize: 16),
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).pop(false);
                                          },
                                        ),
                                      ],
                                    );
                                  });
                            }
                          } on FirebaseAuthException catch (e) {
                            final snackBar =
                                SnackBar(content: Text("${e.message}"));
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          }
                        },
                        child: Text("OTP"))
                  ],
                ),
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Nama Lengkap',
                    style: primaryTextStyle.copyWith(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 7),
                  Container(
                    child: TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                        hintText: 'Masukkan Nama Lengkap',
                        hintStyle: primaryTextStyle.copyWith(fontSize: 16),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    'Email',
                    style: primaryTextStyle.copyWith(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 7),
                  Container(
                    child: TextFormField(
                      controller: _emailController,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                        hintText: 'Masukkan Email',
                        hintStyle: primaryTextStyle.copyWith(fontSize: 16),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    'Nomor Telephone',
                    style: primaryTextStyle.copyWith(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 7),
                  Container(
                    child: TextFormField(
                      controller: _noTelephoneController,
                      onChanged: (value) {
                        setState(() {
                          phone = "$kodephone${_noTelephoneController.text}";
                        });
                      },
                      style: primaryTextStyle.copyWith(fontSize: 16),
                      textAlignVertical: TextAlignVertical.center,
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: EdgeInsets.only(top: 13, left: 10),
                          child: Text(
                            "$kodephone",
                            style: primaryTextStyle.copyWith(fontSize: 16),
                          ),
                        ),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                        hintText: '82387123689',
                        hintStyle: primaryTextStyle.copyWith(fontSize: 16),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    'Password',
                    style: primaryTextStyle.copyWith(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 7),
                  Container(
                    child: TextFormField(
                      obscureText: isShow,
                      controller: _passwordController,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                        hintText: 'Masukkan Password',
                        hintStyle: primaryTextStyle.copyWith(fontSize: 16),
                        suffixIcon: InkWell(
                          onTap: () {
                            showPass1();
                          },
                          child: Icon(
                            isShow
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                            color: secondaryColor,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    'Hobi',
                    style: primaryTextStyle.copyWith(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 7),
                  Container(
                    child: TextFormField(
                      controller: _hobiController,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                        hintText: 'Masukkan Hobi',
                        hintStyle: primaryTextStyle.copyWith(fontSize: 16),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    'Pekerjaan',
                    style: primaryTextStyle.copyWith(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 7),
                  Container(
                    child: TextFormField(
                      controller: _pekerjaanController,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                        hintText: 'Masukkan Pekerjaan',
                        hintStyle: primaryTextStyle.copyWith(fontSize: 16),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide(
                            color: Color(0xffC3C3C3),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
      );
    }

    Widget signup() {
      return ElevatedButton(
          style: ElevatedButton.styleFrom(primary: secondaryColor),
          onPressed: () {
            verifyPhone();
          },

          /*onPressed: () async {
            setState(() {
              isLoading = true;
            });



            if (await authProvider.register(
                name: _nameController.text,
                email: _emailController.text,
                telephone: _noTelephoneController.text,
                password: _passwordController.text,
                hobi: _hobiController.text,
                pekerjaan: _pekerjaanController.text)) {
              Navigator.pushNamedAndRemoveUntil(
                  context, '/navigation', (route) => false);
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text(
                          'Gagal Mendaftar,\nMasukkan email dengan benar!'),
                      titleTextStyle: primaryTextStyle.copyWith(fontSize: 14),
                      actions: <Widget>[
                        TextButton(
                          child: Text(
                            'OK',
                            style: TextStyle(fontSize: 16),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop(false);
                          },
                        ),
                      ],
                    );
                  });
            }

            setState(() {
              isLoading = false;
            });
          },*/
          child: Text(
            'DAFTAR',
            style:
                primaryTextStyle.copyWith(color: gradient2, fontWeight: bold),
          ));
    }

    Widget signin() {
      return Container(
        width: width - 60,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Sudah memiliki akun ? Klik ',
              style: primaryTextStyle.copyWith(
                fontSize: 16,
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, "/login");
              },
              child: Text(
                'Masuk ',
                style: primaryTextStyle.copyWith(
                    fontSize: 16,
                    color: secondaryColor,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  if (!codeSent) header(),
                  content(),
                  SizedBox(
                    height: 20,
                  ),
                  if (!codeSent)
                    Container(
                      height: 50,
                      child: Center(
                          child: isLoading
                              ? Container(
                                  height: 16,
                                  width: 16,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 2,
                                    valueColor:
                                        AlwaysStoppedAnimation(thirdColor),
                                  ),
                                )
                              : signup()),
                    ),
                  SizedBox(
                    height: 20,
                  ),
                  if (!codeSent) signin(),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
