import 'package:edubox/models/allrecipe_model.dart';
import 'package:edubox/models/category_model.dart';
import 'package:edubox/models/detail_recipe.dart';
import 'package:edubox/models/user_model.dart';

import 'package:edubox/services/recipe_service.dart';
import 'package:edubox/widgets/categories.dart';
import 'package:edubox/widgets/menulist.dart';
import 'package:flutter/material.dart';

import '../constans.dart';

class HomePage extends StatefulWidget {
  const HomePage(
      {Key? key, this.user, this.recipes, this.bookpage, this.categories})
      : super(key: key);

  final UserModel? user;
  final List<DetailRecipe?>? recipes;
  final List<CategoryModel?>? categories;
  final Function? bookpage;

  @override
  _HomePageState createState() => _HomePageState();
}

// List<AllRecipeModel?> itemcategories = [];

// Future<void> getCategories() async {
//   try {
//     List<AllRecipeModel?> data = await AllRecipeService().getcategories();
//     itemcategories = data;
//   } catch (e) {
//     print(e);
//   }
// }

class _HomePageState extends State<HomePage> {
  String categoryId = 'makan-malam';

  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget appbar() {
      return PreferredSize(
          child: Container(
            height: 70,
            padding: EdgeInsets.only(left: 20, right: 20, top: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text(
                      'Hello, ',

                      //'Nur',
                      style: primaryTextStyle.copyWith(
                          fontSize: 18, fontWeight: bold),
                    ),
                    Text(
                      "${widget.user!.name}",

                      //'Nur',
                      style: primaryTextStyle.copyWith(
                          fontSize: 18, fontWeight: bold),
                    )
                  ],
                ),
                Container(
                  child: Row(
                    children: [
                      Image.asset(
                        'assets/logo.png',
                        height: 30,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          preferredSize: Size.fromHeight(80));
    }

    Widget header() {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        height: 80,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Temukan Menu\nMasakanmu hari ini?',
              style: primaryTextStyle.copyWith(
                  fontSize: 25, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      );
    }

    Widget category() {
      return Container(
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Row(
                      children: widget.categories!.map((item) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          categoryId = item!.categoryId!;
                        });
                      },
                      child: Container(
                          width: 100,
                          height: 40,
                          margin: EdgeInsets.only(left: 10),
                          decoration: BoxDecoration(
                              color: categoryId == item!.categoryId
                                  ? primaryColor
                                  : gradient2,
                              borderRadius: BorderRadius.circular(10),
                              border:
                                  Border.all(width: 0.5, color: primaryColor)),
                          child: Center(
                            child: Text(
                              "${item.categoryName}",
                              textAlign: TextAlign.center,
                              style: primaryTextStyle.copyWith(
                                  fontSize: 13,
                                  color: categoryId == item.categoryId
                                      ? gradient2
                                      : fourthColor),
                            ),
                          )),
                    );
                  }).toList())),
            ),
            SizedBox(
              height: 20,
            ),
            FutureBuilder<List<AllRecipeModel?>>(
                future: AllRecipeService().getItemsCategory(id: categoryId),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    var itemcategory = snapshot.data!;

                    return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: itemcategory.map((item) {
                              return CategoriesSlideWidget(
                                dataitem: item,
                              );
                            }).toList()),
                      ),
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                })
          ],
        ),
      );
    }

    Widget allRecipe() {
      return Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Recent Menu',
                    style: primaryTextStyle.copyWith(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                    onTap: () async {
                      widget.bookpage!();
                    },
                    child: Text(
                      ' See All',
                      style: primaryTextStyle.copyWith(
                          fontSize: 16, color: thirdColor),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                  children: widget.recipes!.take(5).map((item) {
                return MenuList(
                  data: item,
                );
              }).toList()
                  // MenuList(
                  //   title: "Sop Udang",
                  //   image: "assets/sop udang.png",
                  //   description: "Sop dengan Daging Ayam dan Madu ",
                  // ),
                  // MenuList(
                  //   title: "Sop Udang",
                  //   image: "assets/sop ayam madu.png",
                  //   description: "Sop dengan campuran udang ",
                  // ),
                  ),
            )
          ],
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        appBar: appbar(),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                header(),
                SizedBox(
                  height: 27,
                ),
                category(),
                SizedBox(
                  height: 20,
                ),
                allRecipe(),
                SizedBox(
                  height: 30,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
