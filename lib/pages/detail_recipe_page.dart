import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:edubox/constans.dart';
import 'package:edubox/models/detail_recipe.dart';
import 'package:edubox/pages/navigation.dart';
import 'package:edubox/providers/user_provider.dart';
import 'package:edubox/providers/wishlist_provider.dart';
import 'package:edubox/services/recipe_service.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:photo_view/photo_view.dart';
import 'package:provider/provider.dart';

class DetailsRecipePage extends StatefulWidget {
  DetailsRecipePage({Key? key, this.items, this.image}) : super(key: key);

  final DetailRecipe? items;
  final String? image;

  @override
  _DetailsRecipePageState createState() => _DetailsRecipePageState();
}

class _DetailsRecipePageState extends State<DetailsRecipePage> {
  bool flag = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //getdetailrecipe(widget.id!);
  }

  void _showMaterialDialog(String image) {
    showDialog(
        context: context,
        builder: (context) {
          return Container(
              child: PhotoView(
            imageProvider: NetworkImage(image),
          ));
        });
  }

  FutureOr onGoBack(dynamic value) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    BookmarkProvider bookmarkProvider = Provider.of<BookmarkProvider>(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);

    Widget header() {
      return CachedNetworkImage(
        imageUrl: widget.image!,
        imageBuilder: (context, imageProvider) => InkWell(
          onTap: () {
            _showMaterialDialog(widget.image!);
          },
          child: Container(
            width: double.infinity,
            height: 250.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25)),
              image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
            ),
          ),
        ),
        placeholder: (context, url) => Container(
            width: double.infinity,
            height: 225.0,
            child: CircularProgressIndicator()),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    }

    Widget inggridients() {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              width: width,
              color: fifthhColor,
              child: Text("Ingredients",
                  style: primaryTextStyle.copyWith(
                      fontSize: 20, color: fourthColor, fontWeight: bold)),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: widget.items!.ingredients!
                      .map((item) => Container(
                            padding: EdgeInsets.symmetric(vertical: 3),
                            child: Row(
                              children: [
                                Container(
                                  height: 6,
                                  width: 6,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: fourthColor),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Expanded(
                                  child: Text(
                                    item,
                                    maxLines: 2,
                                    style:
                                        primaryTextStyle.copyWith(fontSize: 14),
                                  ),
                                ),
                              ],
                            ),
                          ))
                      .toList()),
            ),
          ],
        ),
      );
    }

    Widget shortdesc() {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        decoration: BoxDecoration(
            color: fifthhColor, borderRadius: BorderRadius.circular(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //Icon(Icons.bookmark, size: 30, color: secondaryColor),

            GestureDetector(
                onTap: () {
                  //handleAddBookmark();
                  bookmarkProvider.setRecipe(
                      userProvider.newuser!.uid, widget.items!, widget.image);

                  if (bookmarkProvider.isBookmark(widget.items!)) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: secondaryColor,
                        content: Text(
                          'Has been added to the Bookmark',
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                    // badgeProvider.addBadgeBookmarkt();
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: Colors.redAccent,
                        content: Text(
                          'Has been removed from the Bookmark',
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                    //badgeProvider.setBadgeBookmarkt();
                  }
                },
                child: Column(
                  children: [
                    Text(
                      'Bookmark',
                      style: primaryTextStyle.copyWith(
                        fontSize: 14,
                        color: secondaryColor,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Icon(
                        bookmarkProvider.isBookmark(widget.items!)
                            ? Icons.bookmarks
                            : Icons.bookmarks_outlined,
                        color: secondaryColor),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Save',
                      style: primaryTextStyle.copyWith(
                        fontSize: 14,
                        color: secondaryColor,
                      ),
                    )
                  ],
                )),
            Container(
                child: Column(
              children: [
                Text(
                  'Time',
                  style: primaryTextStyle.copyWith(
                    fontSize: 14,
                    color: secondaryColor,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Icon(
                  Icons.timelapse,
                  color: secondaryColor,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "${widget.items!.time}",
                  style: primaryTextStyle.copyWith(
                    fontSize: 14,
                    color: secondaryColor,
                  ),
                )
              ],
            )),
            Container(
                child: Column(
              children: [
                Text(
                  'Author',
                  style: primaryTextStyle.copyWith(
                    fontSize: 14,
                    color: secondaryColor,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Icon(
                  Icons.person,
                  color: secondaryColor,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "${widget.items!.author}",
                  style: primaryTextStyle.copyWith(
                    fontSize: 14,
                    color: secondaryColor,
                  ),
                )
              ],
            )),
          ],
        ),
      );
    }

    Widget description() {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              "${widget.items!.description}",
              maxLines: flag ? 50 : 5,
              overflow: TextOverflow.ellipsis,
              style: primaryTextStyle.copyWith(fontSize: 14),
            ),
            SizedBox(
              height: 5,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  flag = !flag;
                });
              },
              child: Text(
                flag ? "show more" : "show less",
                style: new TextStyle(color: secondaryColor),
              ),
            ),
          ],
        ),
      );
    }

    Widget steps() {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              width: width,
              color: fifthhColor,
              child: Text("Steps",
                  style: primaryTextStyle.copyWith(
                      fontSize: 20, color: fourthColor, fontWeight: bold)),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: widget.items!.steps!.map((item) {
                    int index = widget.items!.steps!.indexOf(item);

                    return Container(
                      padding: EdgeInsets.symmetric(vertical: 3),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 15,
                            child: Text(
                              "${index + 1}.",
                              maxLines: 2,
                              style: primaryTextStyle.copyWith(fontSize: 14),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Text(
                              item,
                              maxLines: 3,
                              style: primaryTextStyle.copyWith(fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                    );
                  }).toList()),
            ),
          ],
        ),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          // appBar: PreferredSize(
          //     child: Container(
          //       padding: EdgeInsets.only(left: 20, right: 20, top: 30),
          //       child: Row(
          //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //         children: [
          //           InkWell(
          //             onTap: () {
          //               Navigator.pop(context);
          //             },
          //             child: Icon(
          //               Icons.arrow_back_ios,
          //               size: 20,
          //             ),
          //           ),
          //           SizedBox(
          //             width: 20,
          //           ),
          //           Expanded(
          //             child: Text(
          //               'Detail Recipe',
          //               style: primaryTextStyle.copyWith(
          //                   fontWeight: bold, fontSize: 20),
          //             ),
          //           ),
          //         ],
          //       ),
          //     ),
          //     preferredSize: Size.fromHeight(80)),
          body: CustomScrollView(
            slivers: [
              SliverAppBar(
                automaticallyImplyLeading: true,
                snap: false,
                title: Text("Detail Recipe",
                    style: primaryTextStyle.copyWith(
                        fontSize: 20, color: gradient2, fontWeight: bold)),
                floating: false,
                centerTitle: true,
                expandedHeight: 250,
                elevation: 5,
                backgroundColor: Colors.white,
                flexibleSpace: FlexibleSpaceBar(
                  background: header(),
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            width: width - (40),
                            child: Text("${widget.items!.title}",
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: primaryTextStyle.copyWith(
                                    fontSize: 20,
                                    color: fourthColor,
                                    fontWeight: bold)),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          shortdesc(),
                          SizedBox(
                            height: 20,
                          ),
                          description(),
                          SizedBox(
                            height: 20,
                          ),
                          inggridients(),
                          SizedBox(
                            height: 15,
                          ),
                          steps(),
                          SizedBox(
                            height: 20,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
