import 'package:edubox/constans.dart';
import 'package:edubox/models/category_model.dart';
import 'package:edubox/models/detail_recipe.dart';
import 'package:edubox/pages/book_page.dart';
import 'package:edubox/pages/home_page.dart';
import 'package:edubox/pages/profile_page.dart';
import 'package:edubox/pages/save_recipe.dart';
import 'package:edubox/providers/recipe_provider.dart';
import 'package:edubox/providers/user_provider.dart';
import 'package:edubox/providers/wishlist_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NavigationPage extends StatefulWidget {
  const NavigationPage({Key? key, this.currentab}) : super(key: key);
  final int? currentab;
  @override
  _NavigationPageState createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  int? _currentTab;
  PageController pageController = PageController();

  @override
  void initState() {
    super.initState();
    _currentTab = widget.currentab ?? 0;
    pageController = PageController(initialPage: _currentTab!);
  }

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = HomePage();

  @override
  Widget build(BuildContext context) {
    UserProvider user = Provider.of<UserProvider>(context);
    RecipeProvider recipeProvider = Provider.of<RecipeProvider>(context);
    List<DetailRecipe?> recipes = recipeProvider.allrecipe;
    List<CategoryModel?> categories = recipeProvider.categories;
    Provider.of<BookmarkProvider>(context, listen: false)
        .getBookmark(user.newuser!.uid!);

    return WillPopScope(
      onWillPop: () async => await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Yakin anda ingin keluar'),
              actions: <Widget>[
                TextButton(
                  child: Text(
                    'Tidak',
                    style: primaryTextStyle.copyWith(fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                TextButton(
                  child: Text(
                    'Ya',
                    style: primaryTextStyle.copyWith(fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      '/login',
                      (route) => false,
                    );
                  },
                ),
              ],
            );
          }),
      child: Scaffold(
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: pageController,
          onPageChanged: (index) {
            setState(() {
              _currentTab = index;
            });
          },
          children: [
            HomePage(
              user: user.newuser,
              recipes: recipes,
              categories: categories,
              bookpage: () {
                _currentTab = 1;
                pageController.jumpToPage(1);
              },
            ),
            BookPage(),
            SaveRecipe(
              homepage: () {
                _currentTab = 0;
                pageController.jumpToPage(0);
              },
            ),
            ProfilePage(
              user: user.newuser,
            )
          ],
        ),

        //PageStorage(bucket: bucket, child: currentScreen),
        bottomNavigationBar: Theme(
          data: ThemeData(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
          ),
          child: ClipRRect(
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(40),
              topLeft: Radius.circular(40),
            ),
            child: BottomAppBar(
              child: Container(
                height: 60,
                width: MediaQuery.of(context).size.width * 0.5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          MaterialButton(
                            onPressed: () {
                              setState(() {
                                _currentTab = 0;
                                pageController.jumpToPage(0);
                              });
                            },
                            minWidth: 40,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.home,
                                  color: _currentTab == 0
                                      ? secondaryColor
                                      : Colors.grey,
                                ),
                                Text('Home')
                              ],
                            ),
                          ),
                          MaterialButton(
                            onPressed: () {
                              setState(() {
                                _currentTab = 1;
                                pageController.jumpToPage(1);
                              });
                            },
                            minWidth: 40,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.book,
                                  color: _currentTab == 1
                                      ? secondaryColor
                                      : Colors.grey,
                                ),
                                Text('Book')
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            child: MaterialButton(
                              onPressed: () {
                                setState(() {
                                  _currentTab = 2;
                                  pageController.jumpToPage(2);
                                });
                              },
                              minWidth: 40,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.bookmark,
                                    color: _currentTab == 2
                                        ? secondaryColor
                                        : Colors.grey,
                                  ),
                                  Text('Save')
                                ],
                              ),
                            ),
                          ),
                          Container(
                            child: MaterialButton(
                              onPressed: () {
                                setState(() {
                                  _currentTab = 3;
                                  pageController.jumpToPage(3);
                                });
                              },
                              minWidth: 40,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.person,
                                    color: _currentTab == 3
                                        ? secondaryColor
                                        : Colors.grey,
                                  ),
                                  Text('profile')
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
