import 'package:edubox/models/detail_recipe.dart';
import 'package:edubox/services/bookmark_service.dart';
import 'package:flutter/foundation.dart';

class BookmarkProvider with ChangeNotifier {
  List<DetailRecipe?> _bookmark = [];

  List<DetailRecipe?> get bookmark => _bookmark;

  set bookmark(List<DetailRecipe?> bookmark) {
    _bookmark = bookmark;
    notifyListeners();
  }

  setRecipe(String? userId, DetailRecipe? recipe, String? images) {
    if (!isBookmark(recipe!)) {
      _bookmark.add(recipe);

      addBookmark(userId: userId, recipe: recipe, images: images);

      notifyListeners();
    } else {
      _bookmark.removeWhere((element) => element!.id == recipe.id);

      delete(userId: userId, product: recipe);
      notifyListeners();
    }
  }

  isBookmark(DetailRecipe? recipe) {
    if (_bookmark.indexWhere((element) => element!.id == recipe!.id) == -1) {
      return false;
    } else {
      return true;
    }
  }

  // isBookmark(ProductModel? product) {
  //   if (_bookmark.indexWhere((element) => element!.id == product!.id) == -1) {
  //     return false;
  //   } else {
  //     return true;
  //   }
  // }

  Future<void> getBookmark(String userId) async {
    try {
      List<DetailRecipe?>? bookmark =
          await BookmarkService().getBookmarkfuture(userId: userId);

      print("cobaprovider" + bookmark.toString());
      _bookmark = bookmark!;
    } catch (e) {
      print(e);
    }
  }

  Future<bool> addBookmark(
      {String? userId, DetailRecipe? recipe, String? images}) async {
    try {
      DetailRecipe? bookmark =
          await BookmarkService().addBookmark(images!, userId!, recipe!);

      _bookmark = bookmark as List<DetailRecipe?>;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> delete({String? userId, DetailRecipe? product}) async {
    try {
      DetailRecipe? bookmark =
          await BookmarkService().delete(userId = userId!, product = product!);

      _bookmark = bookmark as List<DetailRecipe?>;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
