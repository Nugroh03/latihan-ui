import 'package:edubox/models/allrecipe_model.dart';
import 'package:edubox/models/category_model.dart';
import 'package:edubox/models/detail_recipe.dart';
import 'package:edubox/services/recipe_service.dart';
import 'package:flutter/cupertino.dart';

class RecipeProvider with ChangeNotifier {
  List<DetailRecipe?> _allrecipe = [];
  List<CategoryModel?> _categories = [];
  DetailRecipe? _detailRecipe;

  List<DetailRecipe?> get allrecipe => _allrecipe;
  List<CategoryModel?> get categories => _categories;
  DetailRecipe? get detailrecipe => _detailRecipe;

  set allrecipe(List<DetailRecipe?> allrecipe) {
    _allrecipe = allrecipe;
    notifyListeners();
  }

  set categories(List<CategoryModel?> categories) {
    _categories = categories;
    notifyListeners();
  }

  set detailrecipe(DetailRecipe? detailrecipe) {
    _detailRecipe = detailrecipe;
    notifyListeners();
  }

  Future<void> getAllRecipe() async {
    try {
      List<DetailRecipe?> allrecipe = await AllRecipeService().getAllRecipe();
      _allrecipe = allrecipe;
    } catch (e) {
      print(e);
    }
  }

  // Future<void> getAllCategories() async {
  //   try {
  //     List<CategoryModel?> categories =
  //         await AllRecipeService().allcategories();
  //     _categories = categories;
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  Future<void> getCategoriesFirebase() async {
    try {
      List<CategoryModel?>? datacategories =
          await AllRecipeService().getcategoryFirebase();

      _categories = datacategories;
    } catch (e) {
      print(e);
    }
  }

  Future<void> detailitemcategory(String? id) async {
    try {
      print("test1");
      DetailRecipe? detail = await AllRecipeService().getdetailrecipe(id: id);
      print("test1" + detail.toString());
      _detailRecipe = detail;

      print("test2" + _detailRecipe.toString());
    } catch (e) {
      print(e);
    }
  }
}
