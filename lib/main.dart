import 'package:edubox/pages/detail_recipe_page.dart';
import 'package:edubox/pages/signin_page.dart';
import 'package:edubox/pages/navigation.dart';
import 'package:edubox/pages/onboarding_page.dart';
import 'package:edubox/pages/signup_page.dart';
import 'package:edubox/pages/splash_page.dart';
import 'package:edubox/providers/recipe_provider.dart';
import 'package:edubox/providers/user_provider.dart';
import 'package:edubox/providers/wishlist_provider.dart';
import 'package:edubox/scrollweb.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      // options: FirebaseOptions(
      //   apiKey: "AIzaSyAYJy5LwEvpTRSZPE40w-4BiHMeINk3EZY",
      //   appId: "1:954370590714:web:1d8a24d5fd455cc886c643",
      //   messagingSenderId: "954370590714",
      //   projectId: "latihan-edubox-d751b",
      // ),
      );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => UserProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => RecipeProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => BookmarkProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        scrollBehavior: MyCustomScrollBehavior(),
        routes: {
          '/': (context) => SplashPage(),
          '/onboarding': (context) => OnBoardingPage(),
          '/login': (context) => SigninPage(),
          '/register': (context) => RegisterPage(),
          '/navigation': (context) => NavigationPage(),
        },
      ),
    );
  }
}
